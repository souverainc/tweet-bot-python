import tweepy
import xlrd


# VARIABLES
consumer_key = ""
consumer_secret = ""
access_token = ""
access_token_secret = ""
tweet_string = "My tweet."

# AUTHENTIFICATION
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
 
# UPLOADING TWEET
api = tweepy.API(auth)
api.update_status(status=tweet_string)